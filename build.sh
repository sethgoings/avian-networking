#!/bin/bash

function get() {
  wget http://zlib.net/zlib-1.2.5.tar.gz &&
  wget http://www.openssl.org/source/openssl-1.0.0e.tar.gz &&
  wget http://curl.haxx.se/download/curl-7.22.0.tar.gz
}

function build-zlib() {
  tar -zxvf zlib-1.2.5.tar.gz &&
  ( cd zlib-1.2.5 &&
    ./configure --prefix=$(pwd)/../zlib &&
    make &&
    make install )
}

function build-openssl() {
  tar -zxvf openssl-1.0.0e.tar.gz &&
  ( cd openssl-1.0.0e/ &&
    ./config zlib-dynamic shared --prefix=$(pwd)/../openssl &&
    make &&
    make install )
}

function build-curl() {
  tar -zxvf curl-7.22.0.tar.gz &&
  ( cd curl-7.22.0/
    ./configure --with-zlib=$(pwd)/../zlib \
                --with-ssl=$(pwd)/../openssl \
                --prefix=$(pwd)/../curl &&
    export LD_LIBRARY_PATH=$(pwd)/../openssl/lib:$(pwd)/../zlib/lib &&
    make &&
    make install )
}

get
build-zlib
build-openssl
build-curl
